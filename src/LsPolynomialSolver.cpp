#include "LsPolynomialSolver.hpp"
#include <iostream>
#include <cassert>

LsPolynomialSolver::LsPolynomialSolver() {
    /**< This constructor allocates memory to crate an Eigen Vector to store the interpolation results */
    mpComputedData = new Eigen::VectorXd;
}

LsPolynomialSolver::~LsPolynomialSolver() {
    delete mpComputedData;
}

/**< This method stores the least squares approximation coefficients in a vector for a given polynomial degree
 * Let C be the vector of coefficients:
 * A * C = q
 * A = B.Transposed * B
 * q = B.Transposed * Y
 * B = [ b_ij ] = [ x_i ^ j ] for j < n, i < m
 * n: degree of the polynomial
 * m: number of given points
 */
Eigen::VectorXd LsPolynomialSolver::Evaluate(int polynomialDegree) {
    // Check that polynomialDegree is within a valid range
    assert((polynomialDegree >= 0) && (polynomialDegree <= mNumberOfPoints));

    // Create matrix b with mNumberOfPoints rows and polynomialDegree + 1 columns
    Eigen::MatrixXd *b = new Eigen::MatrixXd(mNumberOfPoints, polynomialDegree + 1);

    // Fill matrix b with powers of mData(i, 0)
    for (int i = 0; i < mNumberOfPoints; i++) {
        for (int j = 0; j < polynomialDegree + 1; j++) {
            b->coeffRef(i, j) = pow(mData(i, 0), j);
        }
    }

    //Compute q = b->transpose() * mData.col(1)
    Eigen::VectorXd *q = new Eigen::VectorXd(b->transpose() * mData.col(1));

    //Compute a = b->transpose() * b
    Eigen::MatrixXd *a = new Eigen::MatrixXd(b->transpose() * (*b));

    // Solve the linear system A * C = q to find the least squares approximation coefficients C
    Eigen::VectorXd *coefficients = new Eigen::VectorXd(a->ldlt().solve(*q));

    // Resize mpComputedData to have the same number of elements as coefficients
    mpComputedData->resize(coefficients->size());

    // Store the values in coefficients in mpComputedData
    for (int i = 0; i < coefficients->size(); i++) {
        mpComputedData->coeffRef(i) = coefficients->coeff(i);
    }

    // Delete pointers
    delete b;
    delete q;
    delete a;

    // Return the vector of coefficients
    return *coefficients;
}
