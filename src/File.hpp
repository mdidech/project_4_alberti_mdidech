#ifndef FILEHEADERDEF
#define FILEHEADERDEF

#include <map>
#include <vector>
#include <Eigen/Dense>

/** \brief This mother class is useful only with the use of the daughter classes to specify an object according to the file type.
 * This is the mother class that creates a file object. Itself, it has no type, so it can't be used without it's
 * daughter classes. The daughter classes define which type of data the program is facing and gives the right functions
 * to process them.
 *
 * This class stores the name of the data file as a string, this name should link directly to the right file for it's
 * opening. It also stores a pointer for the Eigen matrix where the data will be stored.
 */
class File {
public:
    virtual void
    TransformToVector(); /**< This is a virtual funcion that is overridden by every daughter class. It's purpose is to transform the data file into a standard eigen matrix which can then be processed with the algorithms. */
    void
    CheckForDuplicates(); /**< This is a function that checks in the eign matrix containing the data that the are not two yi values assigned for a same value of xi.*/
    void SortRowsByFirstColumn();

    void CheckStandardisedVector();

protected:
    std::string mDataFileName; /**< This is the name of the file where the data should be taken from */
    Eigen::MatrixXd *mOutputVectorPointer; /**< This is a pointer to a Eigen matrix where the data will be stored with in the first column the xi and in the second, the yi of the function */
    void removeRow(
            int rowToRemove); /**< This function is capable of removing a row at the rowToRemove index in an eigen matrix and resize it so there is no hole in the data.*/
};

#endif
