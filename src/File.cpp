#include <iostream>
#include "File.hpp"
#include <vector>
#include <Eigen/Dense>
#include <algorithm>

/** \brief virtual class that will be overwritten
 *
 */
void File::TransformToVector() {
    throw (Exception("FILE_TYPE", "Object has no type, cannot transform data."));

}

/** \brief  CheckForDuplicates function checks if the matrix stored at mOutputVectorPointer has 2 yi values assigned to a same xi and removes one of the while throwing a error.
 * It iterates over the matrix and when xi == xj, the row of xj is removed from the matrix and the matrix is resized. An error is thrown.
 */
void File::CheckForDuplicates() {
    int j;
    try {
        for (int i = 0; i < mOutputVectorPointer->rows() - 1; i++) {
            j = i + 1;
            while (j <= mOutputVectorPointer->rows() - 1) {
                if ((mOutputVectorPointer->coeff(i, 0) == mOutputVectorPointer->coeff(j, 0)) && (i !=
                                                                                                 j)) { /**< Checks if the two row indexed by i and j are equal except is I and j are equal, of course */
                    this->removeRow(
                            j); /**< Calling of the removeRow(int rowToRemove) which removes on of the duplicates so the program can continue to run. Don't worry an error is thrown anyway. */
                    //throw (Exception("DATA_DUPLICATE", "The data contains two different yi values assigned to one xi.\n The duplicates are situated at the "+std::to_string(i+1)+"th and "+std::to_string(j+1)+"th data row indexes.\n Please check an rework your data.\n For now, the "+std::to_string(j+1)+"th row is deleted and the computation continues with the reduced data.\n" )); /**< throws an error if the two xi are the same, meaning that one xi coordinate has two different yi values assigned to it. *
                } else {
                    j++;
                }
            }
        }
    }
    catch (Exception &error) {
        error.PrintDebug();
    }
}

/** \brief The removeRow function is a File class function and is capable of removing a row form a eigen matrix at a specific index without losing or having excess data.
 * The functionning of this function is simple, get the matrix dimensions, copy the last row of the matrix over the row
 * that we want to erase. Then, the matrix is resized such as the last row, which is now useless, is not anymore in the dimensions of the matrix.
 * @param rowToRemove This parameter is the index of the row which should be erased form the eigen matrix.
 */
void File::removeRow(int rowToRemove) {
    unsigned int numRows = mOutputVectorPointer->rows();
    unsigned int numCols = mOutputVectorPointer->cols();
    if (rowToRemove < numRows) {
        mOutputVectorPointer->coeffRef(rowToRemove, 0) = mOutputVectorPointer->coeff(numRows - 1, 0);
        mOutputVectorPointer->coeffRef(rowToRemove, 1) = mOutputVectorPointer->coeff(numRows - 1, 1);
    } else {

    }
    mOutputVectorPointer->conservativeResize(numRows - 1, numCols);
    assert(mOutputVectorPointer->rows() == numRows - 1);
}

/** \brief This is a File class function. It is capable to sort the rows of an eigen matrix according to the ascending order of the first column. here, we want to sort the xi values in the first column.
 * This function first creates a vector "vec" of eigen vectors, then stores each row of the eigen matrix to sort, then
 * sorts "vec" according to the first column values and finally overwrites the eigen matrix with the sorted values.
 */
void File::SortRowsByFirstColumn() {
    std::vector<Eigen::VectorXd> vec;
    for (int i = 0; i < mOutputVectorPointer->rows(); ++i) {
        vec.push_back(mOutputVectorPointer->row(i));
    }
    std::sort(vec.begin(), vec.end(),
              [](Eigen::VectorXd const &t1, Eigen::VectorXd const &t2) { return t1(0) < t2(0); });

    for (int i = 0; i < mOutputVectorPointer->rows(); ++i) {
        mOutputVectorPointer->row(i) = vec[i];
    }
}

void File::CheckStandardisedVector() {
    for (int i = 0; i < mOutputVectorPointer->rows() + 1; ++i) {
        if (i != 0) {
            //assertion for unsorted vector
            assert(mOutputVectorPointer->coeff(i - 1, 0) < mOutputVectorPointer->coeff(i, 0));
        }
        for (int j = 0; j < mOutputVectorPointer->rows() + 1; ++j) {
            if (i != j) {
                //assertion for duplicates
                assert(mOutputVectorPointer->coeff(i, 0) != mOutputVectorPointer->coeff(j, 0));
            }
        }
    }
}