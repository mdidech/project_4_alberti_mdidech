#include "ComputeApproximation.hpp"
#include <iostream>

void ComputeApproximation::SetEntryParameters(Eigen::MatrixXd data) {
    mNumberOfPoints = data.rows();
    mData = data;
}

Eigen::VectorXd ComputeApproximation::CopyResults() {
    copyOfResults = *mpComputedData;
    return copyOfResults;
}