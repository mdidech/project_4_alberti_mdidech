#ifndef COMPUTEINTERPOLATIONHEADERDEF
#define COMPUTEINTERPOLATIONHEADERDEF

#include <iostream>
#include <Eigen/Dense>

/** \brief This abstract class allows to choose for which node we want to perform the interpolation and sets up the output string that will be used in the csv output.
 * This is the mother class of "LagrangianSolver" and "BarycentricSolver".
 */

class ComputeInterpolation : public ComputeApproximation {
public:
    // This virtual method will allow to define different algorithms for different daughter classes
    virtual void Evaluate() = 0;

    void CreateOutputString(std::vector<std::string> *p_output_string, std::string algorithm_name);

    void SetNode(double x_interpolated);

    double node;

};

#endif