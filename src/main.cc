#include <iostream>
#include <experimental/filesystem>
#include <fstream>  // ifstream
#include <sstream>  // stringstream
#include <iostream> // cout, endl
#include <iomanip>  // ws
#include <map>      // map
#include <cassert>
#include "ComputeApproximation.hpp"
#include "ComputeApproximation.cpp"
#include "ComputeInterpolation.hpp"
#include "ComputeInterpolation.cpp"
#include "ComputeLeastSquares.hpp"
#include "ComputeLeastSquares.cpp"
#include "LagrangianSolver.hpp"
#include "LagrangianSolver.cpp"
#include "BarycentricSolver.hpp"
#include "BarycentricSolver.cpp"
#include "LsPolynomialSolver.hpp"
#include "LsPolynomialSolver.cpp"
#include "Exception.cpp"
#include "Exception.hpp"
#include "File.cpp"
#include "File.hpp"
#include "CsvFile.cpp"
#include "CsvFile.hpp"
#include "Output.cpp"
#include "Output.hpp"
#include "CsvOutput.cpp"
#include "CsvOutput.hpp"

void CreateSetupMap(std::string file_path, std::map<std::string, std::string> *p_setup_map);

void ProcessData(std::map<std::string, std::string> *data_setup, Eigen::MatrixXd *p_output_data);

void ApplyAlgorithm(std::map<std::string, std::string> *data_setup, Eigen::MatrixXd *p_output_data,
                    std::vector<std::string> *p_output_string_vector);

void OutputTheResults(std::map<std::string, std::string> *data_setup, std::vector<std::string> *p_output_data_string);

int main() {
    //// Creating a map that contains the data file path and the output type wanted
    std::map<std::string, std::string> data_setup;
    CreateSetupMap("Setup", &data_setup);

    //// new vector for storing data
    auto *p_normalized_data = new Eigen::MatrixXd;

    //// Call function that transforms the data into standardized data
    ProcessData(&data_setup, p_normalized_data);

    //// insert the computing, it should use the pointer p_normalized_data as input and maybe give the answer in a well made string that would be stored in the following pointer
    std::vector<std::string> *p_output_string = new std::vector<std::string>;
    ApplyAlgorithm(&data_setup, p_normalized_data, p_output_string);

    //// print the solution and add it in a new file eventually
    OutputTheResults(&data_setup, p_output_string);

    return 0;
}

/** \brief CreateSetupMap reads the setup.txt file and stores the values into a map.
 * This method reads the setup.txt file and writes every line in the map with ":" as arguments delimiter.
 * @param file_path This is a string containing the exact path to the setup file.
 * @param p_setup_map This is a pointer to the map that will be filled by the method. It will contain all the user's chosen parameters.
 */
void CreateSetupMap(std::string file_path, std::map<std::string, std::string> *p_setup_map) {
    std::ifstream setup_stream;
    setup_stream.open(file_path);
    std::string setup_line;
    try {
        if (!setup_stream.is_open()) {
            throw (Exception("SETUP_FILE",
                             "The Setup file could not be opened, please check if it's open in an other app or if it exists or if it's in the wrong folder.\n Maybe the file is not found, it should be in the src folder and it's name should be 'setup'\n If the problem persists, go in clion's 'RUN/DEBUG Configurations' menu and change the 'working directory' to src folder\n"));
        }
    }
    catch (Exception &error) {
        error.PrintDebug();
    }
    if (setup_stream.is_open()) {
        while (getline(setup_stream, setup_line)) { // loop through every line in the file
            std::string key;
            std::string value;
            std::stringstream ss(setup_line); // make a stream from the line
            getline(ss, key, ':'); // read key until :
            ss >> std::ws;              // ignore whitespaces
            getline(ss, value);    // read value until newline
            (*p_setup_map)[key] = value; // Store them
        }
    } else {
        std::cout << "ERROR : Could not open the Setup file. Please check if the file path is correct";
        assert(setup_stream.is_open());
    }
}

/** \brief ProcessData is a method that does every transformation/modification needed on the data so that it can be later fed to the algorithm.
 *  This method checks what type of data the user has specified in the setup and chooses the right type of object to create accordingly.
 *  It then applies the methods TransformToVector(), CheckForDuplicates(), SortRowsByFirstColumn() and CheckStandardisedVector() on the object.
 *  It will throw an error is the input dataset type chosen is not implemented.
 * @param data_setup This is a pointer to the map containing the user's chosen parameters. Here we need the type of the dataset input.
 * @param p_input_data This is a pointer to a Eigen matrix that will be filled with the standardised data created by transforming the data extracted from the dataset.
 */
void ProcessData(std::map<std::string, std::string> *data_setup, Eigen::MatrixXd *p_input_data) {
    //// Different cases dependent of the data file type given
    std::string data_type = (*data_setup)["Data file path from this folder"];
    std::string type_delimiter = ".";
    data_type = data_type.substr(data_type.find(type_delimiter), data_type.length());
    File *p_data_object = nullptr;
    try {
        if (data_type == ".txt") {
            throw (Exception("FILE_TYPE", "The data file type .txt is not yet supported by this program"));
        } else if (data_type == ".csv") {
            p_data_object = new CsvFile((*data_setup)["Data file path from this folder"], p_input_data);
        } else if (data_type == ".xml") {
            throw (Exception("FILE_TYPE", "The data file type .xml is not yet supported by this program"));
        } else {
            throw (Exception("FILE_TYPE", "The data file type is not supported by this program"));
            std::cout << "data type not defined";
        }
    }
    catch (Exception &error) {
        error.PrintDebug();
    }
    p_data_object->TransformToVector();
    p_data_object->CheckForDuplicates();
    p_data_object->SortRowsByFirstColumn();
    p_data_object->CheckStandardisedVector();
}

/** \brief ApplyAlgorithm is a method that computes the algorithm chosen by the user and stores the result in a string vector.
 *  This method checks what algorithm the user wants to use (written in file setup.txt), creates an object depending of that choice and applies some methods to this object.
 *  The methods applied to the object are SetEntryParameters(.), SetNode or SetDegree(), Evaluate() and CreateOutputString.
 * @param data_setup This is a pointer to the map containing the user's chosen parameters. Here we need the algorithm chosen by the user.
 * @param p_input_data This is a pointer to the standardised data, stored in a Eigen matrix.
 * @param p_output_string_vector this is a vector of strings that is used to store the results in the same form as they will be written in the output file. Each cell represents a new line in the output file.
 */
void ApplyAlgorithm(std::map<std::string, std::string> *data_setup, Eigen::MatrixXd *p_input_data,
                    std::vector<std::string> *p_output_string_vector) {
    if ((*data_setup)["Type of algorithm wanted"] == "Lagrangian") {
        LagrangianSolver solver;
        solver.SetEntryParameters(*p_input_data);
        solver.SetNode(stod((*data_setup)["For interpolation, choose the interpolation node"]));
        solver.Evaluate();
        solver.CreateOutputString(p_output_string_vector, (*data_setup)["Type of algorithm wanted"]);
    } else if ((*data_setup)["Type of algorithm wanted"] == "Barycentric") {
        BarycentricSolver solver;
        solver.SetEntryParameters(*p_input_data);
        solver.SetNode(stod((*data_setup)["For interpolation, choose the interpolation node"]));
        solver.Evaluate();
        solver.CreateOutputString(p_output_string_vector, (*data_setup)["Type of algorithm wanted"]);
    } else if ((*data_setup)["Type of algorithm wanted"] == "LsPolynomial") {
        LsPolynomialSolver solver;
        solver.SetEntryParameters(*p_input_data);
        solver.Evaluate(stoi(((*data_setup)["For least squares method, choose Ksi index"])));
        solver.CreateOutputString(p_output_string_vector, (*data_setup)["Type of algorithm wanted"]);
    } else {
        std::cout << "The algorithm is not yet implemented. Please choose one from the following list:" +
                     (*data_setup)["Please choose the wanted algorithm from this list"];
    }
}

/** \brief OutputTheResults creates an output file if not already existing, and overwrites the file with the results.
 * This files checks what's the type of output that the us chose and creates an object accordingly.
 * The object is then subjected to the method WriteToFile() which writes the results in a file of the right type.
 * @param data_setup This is a pointer to the map containing the user's chosen parameters. Here we need the output file type chosen by the user.
 * @param p_output_data_string This is a pointer to the vector of string previously filled in ApplyAlgorithm(), containing the results as the should be written in the output file.
 */
void OutputTheResults(std::map<std::string, std::string> *data_setup, std::vector<std::string> *p_output_data_string) {
    Output *p_output_object = nullptr;
    try {
        if ((*data_setup)["Output type wanted"] == ".csv") {
            p_output_object = new CsvOutput(p_output_data_string);
        } else if ((*data_setup)["Output type wanted"] == ".txt") {
            throw (Exception("OUTPUT_TYPE", "The output file type .txt is not yet supported by this program"));
        } else {
            throw (Exception("OUTPUT_TYPE", "The output file type found in setup is not supported by the program"));
        }
    }
    catch (Exception &error) {
        error.PrintDebug();
    }
    p_output_object->WriteToFile();
}
