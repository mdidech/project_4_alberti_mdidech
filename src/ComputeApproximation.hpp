#ifndef COMPUTEAPPROXIMATIONHEADERDEF
#define COMPUTEAPPROXIMATIONHEADERDEF

#include <iostream>
#include <Eigen/Dense>
#include <vector>

/** \brief This abstract class allows to define the entry parameters before the interpolation/least squares approximation.
 * This is the mother class of "ComputeInterpolation" and "ComputeLeastSquares".
 */
class ComputeApproximation {
public:
    void SetEntryParameters(Eigen::MatrixXd data);

    // This virtual method is only used once the approximation is done, in the daughter classes
    virtual void CreateOutputString(std::vector<std::string> *p_output_string, std::string algorithm_name) = 0;

    Eigen::VectorXd CopyResults();

    Eigen::VectorXd copyOfResults;
protected:
    // Since we want to prevent the user from accessing the entry parameters after he has defined them, we set them as protected and define public inheritance for the daughter classes
    int mNumberOfPoints;
    Eigen::MatrixXd mData;
    Eigen::VectorXd *mpComputedData;
};

#endif