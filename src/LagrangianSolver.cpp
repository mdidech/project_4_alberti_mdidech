#include "LagrangianSolver.hpp"
#include <iostream>

LagrangianSolver::LagrangianSolver() {
    // This constructor allocates memory to crate an Eigen Vector to store the interpolation results
    mpComputedData = new Eigen::VectorXd;
};

LagrangianSolver::~LagrangianSolver() {
    delete mpComputedData;
};

void LagrangianSolver::Evaluate() {
    /**< This method stores the lagrangian coefficients in a vector, and calculates the interpolation.
     * phi_k = [ Product_i ( ( x - x_i ) / ( x_k - x_i ) ) ] for i different from k
     * f(x) = [ Sum_k ( phi_k * y_k ) ] is the approximation for a given node x, calculated in the code by dot product
     * The set of data [ x_k , y_k ] is given.
     * See p.75 of "Scientific Computing with MATLAB and Octave" - A. Quarteroni and F. Saleri */

    // Initialization of the lagrangian coefficients vector, as a pointer in order to delete it from the memory afterwards
    auto p_lagrangian_coefficients = new Eigen::VectorXd;
    (*p_lagrangian_coefficients) = Eigen::VectorXd::Ones(mNumberOfPoints);

    // Computation of the lagrangian coefficients
    for (int k = 0; k < mNumberOfPoints; k++) {
        for (int i = 0; i < mNumberOfPoints; i++) {
            if (i != k) {
                (*p_lagrangian_coefficients)(k) *= (node - mData(i)) / (mData(k, 0) - mData(i, 0));
            }
        }
    }

    // Computation of the lagrangian interpolation as the dot product between lagrangian coefficients and dataset y values vectors
    Eigen::VectorXd lagrangian_interpolation = Eigen::VectorXd::Constant(1, (*p_lagrangian_coefficients).dot(
            mData.col(1)));


    delete p_lagrangian_coefficients;

    // Storage of the coordinate (x,y) of the computed interpolation in mpComputedData
    mpComputedData->resize(2);
    mpComputedData->coeffRef(0) = node;
    mpComputedData->coeffRef(1) = lagrangian_interpolation(0);
}