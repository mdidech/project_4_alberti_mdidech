#ifndef COMPUTELEASTSQUARESHEADERDEF
#define COMPUTELEASTSQUARESHEADERDEF

/** \brief This abstract class allows to calculate the least squares coefficients for any type of functions (polynoms, trigonometric, exponential...).
 * This is the mother class of "LsPolynomialSolver".
 * It allows to implement, as a daughter class, any least squares algorithm and return the coefficients
 */

class ComputeLeastSquares : public ComputeApproximation {
public:
    //This virtual method will allow to define different algorithms for different daughter classes
    virtual Eigen::VectorXd Evaluate(int index_of_ksi) = 0;

    void CreateOutputString(std::vector<std::string> *p_output_string, std::string algorithm_name) override;
};

#endif