#ifndef CSVFILEHEADERDEF
#define CSVFILEHEADERDEF

/** \brief This is a daughter class of File that defines a specific object for csv dataset input
 */

class CsvFile : public File {
public:
    CsvFile(std::string dataFilePath, Eigen::MatrixXd *p_norm_data);

    void TransformToVector() override;
};

#endif
