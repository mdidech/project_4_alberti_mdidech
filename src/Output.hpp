#ifndef OUTPUTHEADERDEF
#define OUTPUTHEADERDEF

#include <map>
#include <vector>
#include <Eigen/Dense>

/** \brief This class allows to do a Lagrangian interpolation of barycentrical form.
 */

class Output {
public:
    virtual void WriteToFile() = 0;

protected:
    std::vector<std::string> *mpOutputString;
};

#endif