#ifndef LEASTSQUARESSOLVERHEADERDEF
#define LEASTSQUARESSOLVERHEADERDEF

#include "ComputeLeastSquares.hpp"

/** \brief This class allows to determine the coefficients of the least square method for a certain polynomial degree.
 */

class LsPolynomialSolver : public ComputeLeastSquares {
public:
    LsPolynomialSolver();

    ~LsPolynomialSolver();

    Eigen::VectorXd Evaluate(int polynomialDegree) override;
};

#endif