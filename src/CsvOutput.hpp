#ifndef CSVOUTPUTHEADERDEF
#define CSVOUTPUTHEADERDEF

/** \brief This is a daughter of the class Output. It implements the method needed to write the result to a csv file
 */

class CsvOutput : public Output {

public:
    CsvOutput(std::vector<std::string> *p_output_string);

    void WriteToFile() override;
};


#endif
