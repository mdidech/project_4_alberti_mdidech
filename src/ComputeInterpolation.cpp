#include "ComputeApproximation.hpp"
#include "ComputeInterpolation.hpp"

#include <iostream>
#include <vector>

void ComputeInterpolation::CreateOutputString(std::vector<std::string> *p_output_string, std::string algorithm_name) {
    // This method creates a string template for the display of the interpolation results, with csv separators
    p_output_string->resize(3); //Initializes the length of the string
    p_output_string->push_back("Values computed with algorithm: ," + algorithm_name +
                               "\n"); //Definition of the first element of the string (shows which algorithm is used)

    //he second and third element of the string respectively represent the setup of the characteristics (node, approximation) and the copy of those values from mpComputedData
    p_output_string->push_back("Node , Approximation \n"); //Definition of the second element of the string
    p_output_string->push_back(
            std::to_string(mpComputedData->coeff(0)) + "," + std::to_string(mpComputedData->coeff(1)) +
            "\n"); // Definition of the third element of the string
}

void ComputeInterpolation::SetNode(double x_interpolated) {
    // This method allows to set the node for which we want to interpolate
    node = x_interpolated;

    assert((node >= mData.col(0).minCoeff()) & (node <= mData.col(
            0).maxCoeff())); // Assertion error if x_interpolated is out of the interval of definition of the existing dataset

}