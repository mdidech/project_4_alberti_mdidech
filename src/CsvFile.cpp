#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <Eigen/Dense>
#include "CsvFile.hpp"

/** \brief CsvFile Constructor
 * @param dataFilePath Stores a string with the exact pathe to the data file input.
 * @param p_norm_data Stores a pointer to a matrix that will be filled with the standardized data.
 */
CsvFile::CsvFile(std::string dataFilePath, Eigen::MatrixXd *p_norm_data) {
    mDataFileName = dataFilePath;
    mOutputVectorPointer = p_norm_data;
}

/** \brief TransformToVector is a function that reads the input .csv data file and stores the values in a vector.
 *  TransformToVector overwrites the virtual function of the mother class File.
 *  CheckDataAndGetSize is called, then,mOutputVectorPointer matrix is resized with the right values,
 *  .csv file with mDataFileName path is read ans stored at mOutputVectorPointer.
 */
void CsvFile::TransformToVector() {
    //// call private function that checks if data is good and gives the size of the data.
    std::vector<int> *data_size = new std::vector<int>(2);
    int noOfRows = 1;
    int noOfColsinit = 2;
    int noOfCols = 0;
    mOutputVectorPointer->resize(noOfRows, noOfCols);
    std::ifstream inputData;
    std::string fileline, filecell;
    inputData.open(mDataFileName);
    noOfRows = 0;
    while (getline(inputData, fileline)) {
        mOutputVectorPointer->conservativeResize(noOfRows + 1, noOfColsinit);
        noOfCols = 0;
        std::stringstream linestream(fileline);
        while (getline(linestream, filecell, ',')) {
            try {
                stod(filecell);
            }
            catch (std::exception &e) {
                std::cout << "EXCEPTION CAUGHT, One of the data file cell cannot be transformed into a double:"
                          << e.what() << std::endl;
            }
            mOutputVectorPointer->coeffRef(noOfRows, noOfCols++) = stod(filecell);
        }
        noOfRows++;
    }
}