#include "BarycentricSolver.hpp"
#include <iostream>

/**
 * \brief This is the constructor for BarycentricSolver. It allows to create a pointer of eigen vector
 */
BarycentricSolver::BarycentricSolver() {
    /**< This constructor allocates memory to crate an Eigen Vector to store the interpolation results */
    mpComputedData = new Eigen::VectorXd;

}

/**
 * \brief This is the destructor for BarycentricSolver. It allows to delete the eigen vector's pointer that was created in the constructor.
 */
BarycentricSolver::~BarycentricSolver() {
    delete mpComputedData;
}

/**
 * \brief This method stores the lagrangian coefficients in a vector, and calculates the interpolation
    * When the node x = x_k, f(x) = f(x_k) = y_k
     *
     * When the node x is different from x_k:
     * f(x) = [ Sum_k top_term_k / weights_over_x_k ]
     * weights_over_x_k = weights_k / (x - x_k)
     * weights_k = [ Product_i ( 1 / x_k - x_i ) ] for i different from k
     * top_term_k = weights_over_x_k * y_k
 */
void BarycentricSolver::Evaluate() {

    //Initializing weights, weights_over_x and top_term as vectors. Used pointers in order to delete them from the memory afterwards
    auto p_weights = new Eigen::VectorXd(mNumberOfPoints);
    auto p_weights_over_x = new Eigen::VectorXd(mNumberOfPoints);
    auto p_top_term = new Eigen::VectorXd(mNumberOfPoints);
    (*p_weights_over_x) = Eigen::VectorXd::Zero(mNumberOfPoints);
    (*p_top_term) = Eigen::VectorXd::Zero(mNumberOfPoints);

    //Calculating weights_k, weights_over_x_k and top_term_k */
    int boundary_index = 0;

    for (int k = 0; k < mNumberOfPoints; k++) {
        for (int i = 0; i < mNumberOfPoints; i++) {
            if (i != k) {
                (*p_weights)(k) *= 1 / (mData(k, 0) - mData(i, 0));
            }
        }
    }

    for (int k = 0; k < mNumberOfPoints; k++) {
        if (node != mData(k, 0)) {
            (*p_weights_over_x)(k) = (*p_weights)(k) / (node - mData(k, 0));
            (*p_top_term)(k) = (*p_weights_over_x)(k) * mData(k, 1);
        } else {
            boundary_index = k;
            break;
        }
    }

    // Initializing the barycentric interpolation as a vector, so it is easier to extend the code later in order to compute it for several nodes and not only one*/
    Eigen::VectorXd barycentric_interpolation;

    // Computing the barycentric interpolation
    // If the interpolated node x_boundaryIndex doesn't belong to the dataset, we compute the barycentric interpolation
    // Otherwise, we just take the associated value f(x_boundary_index) = y_boundary_index */
    if (boundary_index == 0) {
        barycentric_interpolation = Eigen::VectorXd::Constant(1, (*p_top_term).sum() / (*p_weights_over_x).sum());
    } else {
        barycentric_interpolation = Eigen::VectorXd::Constant(1, mData.col(1)(boundary_index));
    }

    delete p_weights;
    delete p_weights_over_x;
    delete p_top_term;

    // Storage of the coordinate (x,y) of the computed interpolation in mpComputedData
    mpComputedData->resize(2);
    mpComputedData->coeffRef(0) = node;
    mpComputedData->coeffRef(1) = barycentric_interpolation(0);

}