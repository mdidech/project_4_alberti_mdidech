#ifndef BARYCENTRICSOLVEREADERDEF
#define BARYCENTRICSOLVEREADERDEF

#include "ComputeInterpolation.hpp"

/** \brief This class allows to do a Lagrangian interpolation of barycentrical form.
 */

class BarycentricSolver : public ComputeInterpolation {
public:
    BarycentricSolver();

    ~BarycentricSolver();

    virtual void Evaluate();
};

#endif