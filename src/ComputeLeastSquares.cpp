#include "ComputeLeastSquares.hpp"
#include <iostream>

void ComputeLeastSquares::CreateOutputString(std::vector<std::string> *p_output_string, std::string algorithm_name) {
    // This method creates a string template for the display of the least squares approximation results, with csv separators
    p_output_string->resize(mpComputedData->size()); /**< Initializes the length of the string */
    p_output_string->push_back("Values computed with algorithm: ," + algorithm_name +
                               "\n"); // Definition of the first element of the string (shows which algorithm is used)

    // The second and third element of the string respectively represent the setup of the characteristics (coefficient, value) and the a0, a1..., ai coefficients
    p_output_string->push_back("coefficient, value \n"); // Definition of the second element of the string
    for (int i = 1; i < mpComputedData->size() + 1; ++i) { //Definition of the third element of the string
        p_output_string->push_back(
                "a" + std::to_string(i - 1) + "," + std::to_string(mpComputedData->coeff(i - 1)) + "\n");
    }
}