#ifndef LAGRANGIANSOLVERHEADERDEF
#define LAGRANGIANSOLVERHEADERDEF

#include "ComputeInterpolation.hpp"

/** \brief This class allows to do a Lagrangian interpolation of basic form.
 */

class LagrangianSolver : public ComputeInterpolation {
public:
    LagrangianSolver();

    ~LagrangianSolver();

    virtual void Evaluate();
};

#endif