#include "CsvOutput.hpp"
#include "Output.hpp"
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <Eigen/Dense>

CsvOutput::CsvOutput(std::vector<std::string> *p_output_string) {
    mpOutputString = p_output_string;
}

/** \brief WriteToFile creates a .csv output file with the computed answers inside.
 * WriteToFile is a function of the CsvOutput class, it overwrites the virtual function WriteToFile of Output class.
 */
void CsvOutput::WriteToFile() {
    std::ofstream output_file;
    output_file.open("Output.csv", std::ios::trunc);
    for (auto &i: *mpOutputString) {
        output_file << i;
    }
    output_file.close();
}