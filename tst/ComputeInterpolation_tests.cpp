#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <Eigen/Dense>
#include <iostream>
#include <cmath>
#include <iostream>
#include "main_test.h"

class MockComputeInterpolation : public ComputeInterpolation {
public:
    MOCK_METHOD0(Evaluate, void());
};

/** Test that setting an interpolation node out of range leads to an assertion error */

TEST(ComputeInterpolation, OutOfRangeNode) {
    /** Create a matrix */
    Eigen::MatrixXd m(3, 2);
    m << 1, 3, 2, 2, 4, 6;

    /** Chose a node x for which we want to interpolate */
    double x = 0.5;

    MockComputeInterpolation compute_interpolation;
    auto v = new Eigen::VectorXd;
    compute_interpolation.SetEntryParameters(m);
    EXPECT_DEATH(compute_interpolation.SetNode(x), "Assertion");
    delete v;
}