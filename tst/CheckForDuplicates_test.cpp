#include <gtest/gtest.h>
#include <Eigen/Dense>
#include <iostream>
#include "main_test.h"

TEST(OneDuplicateTest, DetectAndDeleteDuplicatesCorrectly){
    //define the matrix containing duplicates
    Eigen::MatrixXd* duplicates = new Eigen::MatrixXd;
    duplicates->resize(3, 2);
    duplicates->coeffRef(0, 0) = 3.25;
    duplicates->coeffRef(0, 1) = 2.0;
    duplicates->coeffRef(1, 0) = 3.25;
    duplicates->coeffRef(1, 1) = 1.0;
    duplicates->coeffRef(2, 0) = 10.2;
    duplicates->coeffRef(2, 1) = 3.0;
    // define the sorted matrix
    Eigen::MatrixXd no_more_duplicates;
    no_more_duplicates.resize(2, 2);
    no_more_duplicates.coeffRef(0, 0) = 3.25;
    no_more_duplicates.coeffRef(0, 1) = 2.0;
    no_more_duplicates.coeffRef(1, 0) = 10.2;
    no_more_duplicates.coeffRef(1, 1) = 3.0;
    // create a CsvFile object with the unsorted vector inside
    std::string file_path = "whatever";
    File *p_mock_data_object = nullptr;
    p_mock_data_object = new MockCsvFile(file_path, duplicates);
    // compute the Duplicates detection
    p_mock_data_object->CheckForDuplicates();
    // test if the duplicates are deleted
    for (int i = 0; i < no_more_duplicates.rows(); ++i) {
        for (int j = 0; j < no_more_duplicates.cols(); ++j) {
            EXPECT_EQ(duplicates->coeff(i,j),no_more_duplicates.coeff(i,j));
        }
    }
    EXPECT_EQ(duplicates->rows(),no_more_duplicates.rows());
    EXPECT_EQ(duplicates->cols(),no_more_duplicates.cols());
    EXPECT_NE(duplicates->coeff(2,0),NULL);
}

TEST(NoDuplicateTest, DetectAndDeleteDuplicatesCorrectly){
    //define the matrix containing duplicates
    Eigen::MatrixXd* duplicates = new Eigen::MatrixXd;
    duplicates->resize(3, 2);
    duplicates->coeffRef(0, 0) = 3.25;
    duplicates->coeffRef(0, 1) = 2.0;
    duplicates->coeffRef(1, 0) = 4.32;
    duplicates->coeffRef(1, 1) = 1.0;
    duplicates->coeffRef(2, 0) = 10.2;
    duplicates->coeffRef(2, 1) = 3.0;
    // define the sorted matrix
    Eigen::MatrixXd no_more_duplicates;
    no_more_duplicates.resize(3, 2);
    no_more_duplicates.coeffRef(0, 0) = 3.25;
    no_more_duplicates.coeffRef(0, 1) = 2.0;
    no_more_duplicates.coeffRef(1, 0) = 4.32;
    no_more_duplicates.coeffRef(1, 1) = 1.0;
    no_more_duplicates.coeffRef(2, 0) = 10.2;
    no_more_duplicates.coeffRef(2, 1) = 3.0;
    // create a CsvFile object with the unsorted vector inside
    std::string file_path = "whatever";
    File *p_mock_data_object = nullptr;
    p_mock_data_object = new MockCsvFile(file_path, duplicates);
    // compute the Duplicates detection
    p_mock_data_object->CheckForDuplicates();
    // test if the duplicates are deleted
    for (int i = 0; i < no_more_duplicates.rows(); ++i) {
        EXPECT_EQ(duplicates->coeff(i,0),no_more_duplicates.coeff(i,0));
        EXPECT_EQ(duplicates->coeff(i,1),no_more_duplicates.coeff(i,1));
    }
    EXPECT_EQ(duplicates->rows(),no_more_duplicates.rows());
    EXPECT_EQ(duplicates->cols(),no_more_duplicates.cols());
}

TEST(VectorDuplicateTest, DetectAndDeleteDuplicatesCorrectly){
    //define the matrix containing duplicates
    Eigen::MatrixXd* duplicates = new Eigen::MatrixXd;
    duplicates->resize(4, 2);
    duplicates->coeffRef(0, 0) = 5.275;
    duplicates->coeffRef(0, 1) = 2.0;
    duplicates->coeffRef(1, 0) = 5.275;
    duplicates->coeffRef(1, 1) = 1.0;
    duplicates->coeffRef(2, 0) = 5.275;
    duplicates->coeffRef(2, 1) = 3.0;
    duplicates->coeffRef(3, 0) = 5.275;
    duplicates->coeffRef(3, 1) = 4.0;
    // define the sorted matrix
    Eigen::MatrixXd no_more_duplicates;
    no_more_duplicates.resize(1, 2);
    no_more_duplicates.coeffRef(0, 0) = 5.275;
    no_more_duplicates.coeffRef(0, 1) = 2.0;
    // create a CsvFile object with the unsorted vector inside
    std::string file_path = "whatever";
    File *p_mock_data_object = nullptr;
    p_mock_data_object = new MockCsvFile(file_path, duplicates);
    // compute the Duplicates detection
    p_mock_data_object->CheckForDuplicates();
    // test if the duplicates are deleted

    for (int i = 0; i < no_more_duplicates.rows(); ++i) {
        for (int j = 0; j < no_more_duplicates.cols(); ++j) {
            //EXPECT_EQ(duplicates->coeff(i,j),no_more_duplicates.coeff(i,j));
        }
        std::cout<<duplicates->coeff(i,0)<<" , "<<duplicates->coeff(i,1)<<"\n";
    }

    for (int i = 0; i < duplicates->rows(); ++i) {
        std::cout<<duplicates->coeff(i,0)<<" , "<<duplicates->coeff(i,1)<<"\n";
    }
    EXPECT_EQ(duplicates->rows(),no_more_duplicates.rows());
    EXPECT_EQ(duplicates->cols(),no_more_duplicates.cols());
}

