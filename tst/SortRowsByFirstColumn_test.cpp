#include <gtest/gtest.h>
#include <Eigen/Dense>
#include <iostream>
#include "main_test.h"

// partially unsorted matrix test
TEST(PartiallyUnsortedTest, SortsVectorCorrectly){
    //define the unsorted matrix
    Eigen::MatrixXd* unsorted = new Eigen::MatrixXd;
    unsorted->resize(3, 2);
    unsorted->coeffRef(0, 0) = 3.25;
    unsorted->coeffRef(0, 1) = 2.0;
    unsorted->coeffRef(1, 0) = -1.3;
    unsorted->coeffRef(1, 1) = 1.0;
    unsorted->coeffRef(2, 0) = 10.2;
    unsorted->coeffRef(2, 1) = 3.0;
    // define the sorted matrix
    Eigen::MatrixXd sorted;
    sorted.resize(3, 2);
    sorted.coeffRef(0, 0) = -1.3;
    sorted.coeffRef(0, 1) = 1.0;
    sorted.coeffRef(1, 0) = 3.25;
    sorted.coeffRef(1, 1) = 2.0;
    sorted.coeffRef(2, 0) = 10.2;
    sorted.coeffRef(2, 1) = 3.0;
    // create a CsvFile object with the unsorted vector inside
    std::string file_path = "whatever";
    File *p_mock_data_object = nullptr;
    p_mock_data_object = new MockCsvFile(file_path, unsorted);
    // compute the sorting
    p_mock_data_object->SortRowsByFirstColumn();
    // test if the vector is well sorted
    for (int i = 0; i < sorted.rows(); ++i) {
        for (int j = 0; j < sorted.cols(); ++j) {
            EXPECT_EQ(unsorted->coeff(i,j),sorted.coeff(i,j));
        }
    }
}
// already sorted matrix test
TEST(AlreadySortedTest, SortsVectorCorrectly){
    //define the already sorted matrix
    Eigen::MatrixXd* unsorted = new Eigen::MatrixXd;
    unsorted->resize(3, 2);
    unsorted->coeffRef(0, 0) = -1.3;
    unsorted->coeffRef(0, 1) = 1.0;
    unsorted->coeffRef(1, 0) = 3.25;
    unsorted->coeffRef(1, 1) = 2.0;
    unsorted->coeffRef(2, 0) = 10.2;
    unsorted->coeffRef(2, 1) = 3.0;
    // define the sorted matrix
    Eigen::MatrixXd sorted;
    sorted.resize(3, 2);
    sorted.coeffRef(0, 0) = -1.3;
    sorted.coeffRef(0, 1) = 1.0;
    sorted.coeffRef(1, 0) = 3.25;
    sorted.coeffRef(1, 1) = 2.0;
    sorted.coeffRef(2, 0) = 10.2;
    sorted.coeffRef(2, 1) = 3.0;
    // create a CsvFile object with the unsorted vector inside
    std::string file_path = "whatever";
    File *p_mock_data_object = nullptr;
    p_mock_data_object = new MockCsvFile(file_path, unsorted);
    // compute the sorting
    p_mock_data_object->SortRowsByFirstColumn();
    // test if the vector is well sorted
    for (int i = 0; i < sorted.rows(); ++i) {
        for (int j = 0; j < sorted.cols(); ++j) {
            EXPECT_EQ(unsorted->coeff(i,j),sorted.coeff(i,j));
        }
    }
}
// completely unsorted matrix test
TEST(CompletelyUnsortedTest, SortsVectorCorrectly){
    //define the unsorted matrix
    Eigen::MatrixXd* unsorted = new Eigen::MatrixXd;
    unsorted->resize(3, 2);
    unsorted->coeffRef(0, 0) = 3.25;
    unsorted->coeffRef(0, 1) = 2.0;
    unsorted->coeffRef(1, 0) = 10.2;
    unsorted->coeffRef(1, 1) = 3.0;
    unsorted->coeffRef(2, 0) = -1.3;
    unsorted->coeffRef(2, 1) = 1.0;
    // define the sorted matrix
    Eigen::MatrixXd sorted;
    sorted.resize(3, 2);
    sorted.coeffRef(0, 0) = -1.3;
    sorted.coeffRef(0, 1) = 1.0;
    sorted.coeffRef(1, 0) = 3.25;
    sorted.coeffRef(1, 1) = 2.0;
    sorted.coeffRef(2, 0) = 10.2;
    sorted.coeffRef(2, 1) = 3.0;
    // create a CsvFile object with the unsorted vector inside
    std::string file_path = "whatever";
    File *p_mock_data_object = nullptr;
    p_mock_data_object = new MockCsvFile(file_path, unsorted);
    // compute the sorting
    p_mock_data_object->SortRowsByFirstColumn();
    // test if the vector is well sorted
    for (int i = 0; i < sorted.rows(); ++i) {
        for (int j = 0; j < sorted.cols(); ++j) {
            EXPECT_EQ(unsorted->coeff(i,j),sorted.coeff(i,j));
        }
    }
}