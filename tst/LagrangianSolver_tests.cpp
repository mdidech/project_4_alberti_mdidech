#include <gtest/gtest.h>
#include <Eigen/Dense>
#include <iostream>
#include <cmath>
#include <iostream>
#include "main_test.h"

/** Test the Lagrangian interpolation on an existing node */

TEST(LagrangianSolver, ExistingNode) {
    /** Create a matrix */
    Eigen::MatrixXd m(3, 2);
    m << 1, 3, 2, 2, 4, 6;

    /** Choose a node x for which we want to interpolate */
    double x = 4;

    /** Compute the interpolated y */
    LagrangianSolver solver;
    solver.SetEntryParameters(m);
    solver.SetNode(x);
    solver.Evaluate();
    double y_interpolated = solver.CopyResults()(1);
    /** Check that the interpolated y value is correct */
    EXPECT_NEAR(y_interpolated, 6, 1e-6);
}

/** Test the Lagrangian interpolation on a simple node */

TEST(LagrangianSolver, SimpleNode) {
    /** Create a matrix */
    Eigen::MatrixXd m(3, 2);
    m << 1, 3, 2, 2, 4, 6;

    /** Chose a node x for which we want to interpolate */
    double x = 3;

    /** Compute the interpolated y */
    LagrangianSolver solver;
    solver.SetEntryParameters(m);
    solver.SetNode(x);
    solver.Evaluate();
    double y_interpolated = solver.CopyResults()(1);

    /** Compute the analytical y
     *  The Lagrangian polynome is f(x) = x^2 - 4x + 6
     *  (see Example 3.7 of Numerical Analysis and Computational Mathematics - Dr. Luca Dede) */

    double y_analytical = pow(x, 2) - 4 * x + 6;

    /** Check that the interpolated y value is correct */
    EXPECT_NEAR(y_interpolated, y_analytical, 1e-6);
}

/** Test the Lagrangian interpolation where one of the coordinates is close to zero */

TEST(LagrangianSolver, CloseToZeroNode) {
/** Create a matrix */
    Eigen::MatrixXd m(2, 2);
    m << 0.0001, 3, 0.0002, 1;

/** Chose a node x for which we want to interpolate */
    double x = 0.00015;

/** Compute the interpolated y */
    LagrangianSolver solver;
    solver.SetEntryParameters(m);
    solver.SetNode(x);
    solver.Evaluate();
    double y_interpolated = solver.CopyResults()(1);

/** Compute the analytical y
 * The Lagrangian polynome is f(x) = 3(x-0.0002)/(0.0001-0.0002) + (x-0.0001)/(0.0002-0.0001)
 * (see Example 3.7 of Numerical Analysis and Computational Mathematics - Dr. Luca Dede)
 */
    double y_analytical = 3 * (x - 0.0002) / (0.0001 - 0.0002) + (x - 0.0001) / (0.0002 - 0.0001);

/** Check that the interpolated y value is correct */
    EXPECT_NEAR(y_interpolated, y_analytical, 1e-6);
}
