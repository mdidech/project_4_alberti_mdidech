#ifndef PCSC_MAIN_TEST_H
#define PCSC_MAIN_TEST_H
#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <Eigen/Dense>
#include <iostream>
#include <cmath>
#include <iostream>
#include <experimental/filesystem>
#include <fstream>  // ifstream
#include <sstream>  // stringstream
#include <iostream> // cout, endl
#include <iomanip>  // ws
#include <map>      // map
#include <cassert>
#include "ComputeApproximation.hpp"
#include "ComputeApproximation.cpp"
#include "ComputeInterpolation.hpp"
#include "ComputeInterpolation.cpp"
#include "ComputeLeastSquares.hpp"
#include "ComputeLeastSquares.cpp"
#include "LagrangianSolver.hpp"
#include "LagrangianSolver.cpp"
#include "BarycentricSolver.hpp"
#include "BarycentricSolver.cpp"
#include "LsPolynomialSolver.hpp"
#include "LsPolynomialSolver.cpp"
#include "Exception.cpp"
#include "File.cpp"
#include "File.hpp"
#include "CsvFile.hpp"
#include "CsvFile.cpp"
#include "Output.hpp"
#include "Output.cpp"
#include "CsvOutput.hpp"
#include "CsvOutput.cpp"

class MockFile : public File {
public:
    MOCK_METHOD0(SortByFirstColumn, void());

};
class MockCsvFile : public File {
    MOCK_METHOD0(SortByFirstColumn, void());
public:
    MockCsvFile(std::string dataFilePath, Eigen::MatrixXd* p_norm_data) {
        mDataFileName = dataFilePath;
        mOutputVectorPointer = p_norm_data;
    }
};
class MyClassTest : public ::testing::Test {
 protected:
  MockFile file_mock;
};


#endif //PCSC_MAIN_TEST_H
